﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>Untitled 1</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

</head>
<link rel="STYLESHEET" type="text/css" href="estilos.css"></link>
        
<body>
<div class="contenedor">

  <?php
  	include("menu.php");
  ?>


	
	
	    	 
	<article>
            <div class="contenedor-animacion">
                <img class="moto-animada" src="imagenes/moto_animada.png" width="5%"/>
                
            </div>
	</article>
	
	<section>
			<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				  <div class="carousel-inner">
				    <div class="carousel-item active">
				      <img class="d-block w-100" src="imagenes/slider/slider1.jpg" alt="First slide" />
				    </div>
				    <div class="carousel-item">
				      <img class="d-block w-100" src="imagenes/slider/slider2.jpg" alt="Second slide" />
				    </div>
				    <div class="carousel-item">
				      <img class="d-block w-100" src="imagenes/slider/slider3.jpg" alt="Third slide" />
				    </div>
				    <div class="carousel-item">
				      <img class="d-block w-100" src="imagenes/slider/slider4.jpg" alt="Third slide" />
				    </div>
					<div class="carousel-item">
				      <img class="d-block w-100" src="imagenes/slider/slider5.jpg" alt="Third slide" />
				    </div>
					<div class="carousel-item">
				      <img class="d-block w-100" src="imagenes/slider/slider6.jpg" alt="Third slide" />
				    </div>
					<div class="carousel-item">
				      <img class="d-block w-100" src="imagenes/slider/slider7.jpg" alt="Third slide" />
				    </div>
					<div class="carousel-item">
				      <img class="d-block w-100" src="imagenes/slider/slider8.jpg" alt="Third slide" />
				    </div>
				  </div>
				  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
		</div>
	
		
	</section>
	
	
	<footer>
		<div class="contenedor-fb1"	>
			<span id="fb1">MANTENTE INFORMADO</span>
			<div class="contenedor-redes">
				<i class="fa fa-facebook-square fa-2x"></i>
				<i class="fa fa-twitter fa-2x"></i>
				<i class="fa fa-instagram fa-2x"></i>
				<i class="fa fa-youtube fa-2x"></i>				
			</div>
			
		</div>
		<div class="contenedor-fb2"	>
			
			<div class="fb2">ACERCA DE NUESTRA EMPRESA
				<a href="#">Nuestra Empresa</a>
				<a href="#">Empleo</a>
				<a href="#">Inversores</a>
				<a href="#">Sostenibilidad</a>
				<a href="#">Noticias</a>
				<a href="#">Oficial</a>
				<a href="#">Visitar la fábrica</a>
				<a href="#">Contáctanos</a>

			</div>
		</div>
		
		<div class="contenedor-fb3"	>
			
			<div class="fb3">Recursos
				<a href="#">Servicios Financieros</a>
				

			</div>
		</div>

		
	</footer>
</div>



</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.js" />


</html>


