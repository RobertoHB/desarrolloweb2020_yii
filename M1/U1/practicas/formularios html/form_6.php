<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>Formulario de registro</title>

</head>

<body>
<h2>Formulario de registro</h2>
<form action="" method="get">
	<fieldset>
		<legend>¿Quien eres?</legend>
		
		<label>Nombre *</label>
		<input type="text" name="nombre" required/>
		<label>Apellidos *</label>
		<input type="text" name="apellidos" required /><br/>
		
		<label>Correo electrónico *</label>
		<input type="email" name="correo" required />
		
		<label>Fecha de nacimiento</label>
		<input type="date" name="nacimiento" />
		
	</fieldset>

	<fieldset>
		<legend>¿De dónde eres?</legend>
		
		<label for="ciudad">Ciudad</label>
		<input type="text" name="ciudad" /><br/>
		<label for="cp">Código Postal</label>
		<input type="text" name="cp" maxlength="5" /><br/>
		
		<label for="poblaciones">Provincia: </label>
		<select name="poblaciones">
		  <option value="1">Asturias</option> 
		  <option value="2">Cantabria</option>
		  <option value="3">País Vasco</option>
		  <option value="4">Navarra</option>
		</select> <br><br>		
	</fieldset>
	
	<fieldset>
		<legend>¿Cómo quieres inciar sesión ?</legend>
		
		<label>Nombre de usuario</label>
		<input type="text" name="usuario" required /><br/>
		<label>Contraseña</label>
		<input type="password" name="pas" required /><br/>
		<label>Repetir contraseña</label>
		<input type="password" name="repas" required />
		
	</fieldset>

	<fieldset>
		<legend>Condiciones de registro</legend>
		
		<label>Deseo recibir ofertas de iDESWEB</label>
		<input type="radio" value=""/>Una vez al día
		<input type="radio" value=""/>Una vez a la semana
		<input type="radio" value=""/>Una vez al mes<br/>
		
		<input type="checkbox" name="acepto" id ="acepto" onclick="mostrar_boton()" />Acepto el acuerdo de servicios, la declaración de privacidad y la declaración de uso de cookies
		
	</fieldset>

	
	<input type="submit" name="btn_cta" id="btn_cta" value="Crear cuenta" style="display:none" />
<script type="text/javascript">

function mostrar_boton(){
alert("hola");
var acepto = document.getElementById("acepto");
var btn_creo_cta = document.getElementById("btn_cta");


if (acepto == true){
	bn_creo_cta.style.visibility = "visible";
}else{
	bn_creo_cta.style.visibility = "hidden";
	
}

</script>


</form>

</body>


</html>
